object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 336
  ClientWidth = 605
  Color = clGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BT_L: TButton
    Left = 256
    Top = 40
    Width = 75
    Height = 33
    Caption = 'Lenght'
    TabOrder = 0
    OnClick = BT_LClick
  end
  object BT_C: TButton
    Left = 256
    Top = 79
    Width = 75
    Height = 34
    Caption = 'Contains'
    TabOrder = 1
    OnClick = BT_CClick
  end
  object BT_T: TButton
    Left = 256
    Top = 119
    Width = 75
    Height = 35
    Caption = 'Trim'
    TabOrder = 2
    OnClick = BT_TClick
  end
  object BT_LK: TButton
    Left = 256
    Top = 160
    Width = 75
    Height = 34
    Caption = 'LowerCase'
    TabOrder = 3
    OnClick = BT_LKClick
  end
  object BT_UK: TButton
    Left = 256
    Top = 200
    Width = 75
    Height = 34
    Caption = 'UpperCase'
    TabOrder = 4
    OnClick = BT_UKClick
  end
  object BT_R: TButton
    Left = 256
    Top = 240
    Width = 75
    Height = 41
    Caption = 'Replace'
    TabOrder = 5
    OnClick = BT_RClick
  end
  object Memo: TMemo
    Left = 8
    Top = 40
    Width = 242
    Height = 241
    Lines.Strings = (
      'Memo')
    TabOrder = 6
  end
  object Memo2: TMemo
    Left = 337
    Top = 40
    Width = 240
    Height = 241
    Lines.Strings = (
      'Memo2')
    TabOrder = 7
  end
end
